#!/usr/bin/env python
import eventlet
eventlet.monkey_patch()
from threading import Lock
from flask import Flask, render_template, session, request, \
    copy_current_request_context
from flask_socketio import SocketIO, emit, join_room, leave_room, \
    close_room, rooms, disconnect, Namespace
from pblib import amqp
import json
from flask_cors import CORS
import speech_recognition as sr
from pblib.helpers import get_consumer_id_by_custom_id


# Set this variable to "threading", "eventlet" or "gevent" to test the
# different async modes, or leave it set to None for the application to choose
# the best option based on installed packages.
async_mode = "eventlet"

app = Flask(__name__, static_folder='static', static_url_path='')
app.config['SECRET_KEY'] = 'secret!'
amqp_data = amqp.getAmqpData()
rabbit_connection_url = "amqp://" + str(amqp_data.get("user")) + ":" + str(amqp_data.get("password")) + "@" + str(
    amqp_data.get(
        "host")) + ":" + str(amqp_data.get("port")) + str(amqp_data.get("vhost"))


# Documentation: https://flask-socketio.readthedocs.io/en/latest/
socketio = SocketIO(app, cookie=False, async_mode=async_mode, message_queue=rabbit_connection_url, engineio_logger=False, origins='*', cors_allowed_origins=[])
external_socketio = SocketIO(message_queue=rabbit_connection_url)

app.config['CORS_SUPPORTS_CREDENTIALS'] = True
CORS(app, resources={r"/*": {"origins": "*"}}, automatic_options=True)


thread = None
thread_lock = Lock()


def pbevent_handler(ch, method=None, properties=None, body=None):
    try:
        payload = body.decode()
        room = json.loads(payload).get("room")
        if method.routing_key == "commands":
            external_socketio.emit('command', payload, room=room, json=True)
        elif method.routing_key == "events":
            external_socketio.emit('event', payload, room=room, json=True)
        elif method.routing_key == "log":
            external_socketio.emit('log', payload, room=room, json=True)
        elif method.routing_key == "notifications":
            external_socketio.emit('notification', payload, room=room, json=True)
        else:
            print("NOT YET IMPLEMENTED", method.routing_key)
        ch.basic_ack(method.delivery_tag)
    except Exception as e:
        print(e)

    return True

name = "socketserver"
queue = "socketserver"
bindings = {}
bindings['socketserver'] = ['notifications', 'commands', 'log', 'events']
amqp.client.addConsumer(name, queue, bindings, pbevent_handler)


def background_thread():
    """Example of how to send server generated events to clients."""
    count = 0
    while True:
        socketio.sleep(3)
        count += 1
        socketio.emit('my_response',
                      {'data': 'Server generated event', 'count': count},
                      namespace='/')


@app.route('/')
def index():
    return render_template('index.html', api_url="http://localhost", async_mode=socketio.async_mode)


@app.route('/send')
def send():
    message = request.args.get("msg")
    room = request.args.get("room")
    session['receive_count'] = session.get('receive_count', 0) + 1
    socketio.emit('my_response',
                  {'data': message, 'count': session['receive_count']},
                  room=room)
    return "ok"


@socketio.on('notification', namespace='/')
def notification(message):
    print("MY MESSAGE!!!")
    emit('notification', message, room=message['room'])

@socketio.on('consolelog', namespace='/')
def consolelog(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('consolelog', message, room="consolelog")



@socketio.on('screenshare', namespace='/')
def screenshare(message):
    socketio.emit('streamdata', message, room="screenshare")



@socketio.on('command', namespace='/')
def command(payload):
    emit('command', payload, room=payload['room'])

@socketio.on('log', namespace='/')
def log(payload):
    print("LOGMESSAGE", payload)
    emit('log', payload, room=payload['room'])

@socketio.on('analytics', namespace='/')
def analytics(payload):
    print("ANALYTICS", payload)
    amqp.client().publish(toExchange="analytics", routingKey="analytics", message=payload)
    #emit('analytics', payload, room=payload['room'])


@socketio.on('my_event', namespace='/')
def test_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']})


@socketio.on('my_broadcast_event', namespace='/')
def test_broadcast_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         broadcast=True)


@socketio.on('join', namespace='/')
def join(message):
    if type(message) == str:
        message = json.loads(message)
    print("JOINING ROOM", message, request.headers.get("X-Consumer-Groups"))
    if message['room'] == request.headers.get("X-Consumer-Custom-Id") or "tmp_" in message['room'] or message['room'] in request.headers.get("X-Consumer-Groups") or is_user_of_consumer(message['room'], request.headers.get("X-Consumer-Custom-Id")):
        join_room(message['room'])
        session['receive_count'] = session.get('receive_count', 0) + 1
        emit('my_response',
             {'data': 'In rooms: ' + ', '.join(rooms()),
              'count': 1})
    else:
        emit('my_response',
             {'data': 'You are not allowed to join this room: ' + str(message['room']),
              'count': 1})



@socketio.on('leave', namespace='/')
def leave(message):
    leave_room(message['room'])
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': 'In rooms: ' + ', '.join(rooms()),
          'count': session['receive_count']})


@socketio.on('close_room', namespace='/')
def close(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response', {'data': 'Room ' + message['room'] + ' is closing.',
                         'count': session['receive_count']},
         room=message['room'])
    close_room(message['room'])


@socketio.on('my_room_event', namespace='/')
def send_room_message(message):
    session['receive_count'] = session.get('receive_count', 0) + 1
    emit('my_response',
         {'data': message['data'], 'count': session['receive_count']},
         room=message['room'])




@socketio.on('disconnect_request', namespace='/')
def disconnect_request():
    @copy_current_request_context
    def can_disconnect():
        disconnect()

    session['receive_count'] = session.get('receive_count', 0) + 1
    # for this emit we use a callback function
    # when the callback function is invoked we know that the message has been
    # received and it is safe to disconnect
    emit('my_response',
         {'data': 'Disconnected!', 'count': session['receive_count']},
         callback=can_disconnect)


@socketio.on('my_ping', namespace='/')
def ping_pong():
    emit('my_pong')


@socketio.on('connect', namespace='/')
def test_connect():
    global thread
    #with thread_lock:
    #    if thread is None:
    #        thread = socketio.start_background_task(background_thread)
    emit('my_response', {'data': 'Connected', 'count': 0})
    emit('command', {'command': 'fireBackendEvent', 'args': {"eventname": "SIOConnected", "eventdata": {}}})


@socketio.on('disconnect', namespace='/')
def test_disconnect():
    print('Client disconnected', request.sid)

import os
import requests

transcriptionResults = {}
@socketio.on('streamForTranscription', namespace='/')
def save_speech(message):
    if transcriptionResults.get(message.get("task_id")) is not None:
        print("Speech Recognition result from cache: " + transcriptionResults.get(message.get("task_id")))
        emit("speechRecognitionResult", {"task_id": message.get("task_id"), "source_event": message.get("source_event"), "result": transcriptionResults.get(message.get("task_id"))})
    else:
        path = "/src/app/static/"+str(message.get("task_id"))+".wav"
        fp = open(path, "wb")
        fp.write(message.get("data_blob"))
        fp.close()
        #emit('playresponse', message.get("data_blob"))

        r = sr.Recognizer()
        with sr.AudioFile(path) as source:
            audio = r.record(source)  # read the entire audio file

        try:
            result = r.recognize_google(audio, language='de-DE')
            print("Google Speech Recognition thinks you said " + result)
        except Exception as e:
            print("Google Speech Recognition was unable to transcribe")
            result = False

        transcriptionResults[message.get("task_id")] = result
        emit("speechRecognitionResult", {"task_id": message.get("task_id"), "source_event": message.get("source_event"), "result": result})
        os.remove(path)



def is_user_of_consumer(user ,consumer_id):
    if user in getLoginsOfConsumer(consumer_id):
        return True
    else:
        return False

def getLoginsOfConsumer(consumer_id):
    gateway_consumer_id = get_consumer_id_by_custom_id(consumer_id)
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers/" + str(gateway_consumer_id) + "/basic-auth"
    print(url)
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, headers=headers)

    print("RESPONSE", response.json().get("data", []))

    logins = [item.get("username") for item in response.json().get("data", [])]

    return logins


if __name__ == '__main__':
    from autorun import Autorun
    ar = Autorun()
    ar.post_deployment()
    socketio.run(app, host='0.0.0.0', debug=True)
