import requests
import json
import os

def singleton(class_):
    instances = {}
    def getinstance(*args, **kwargs):
        if class_ not in instances:
            instances[class_] = class_(*args, **kwargs)
        return instances[class_]
    return getinstance


def filter_dict(filter, dictionary):

    for key, value in filter.items():
        if isinstance(value, dict):
            # get node or create one
            node = dictionary.setdefault(key, {})
            filter_dict(value, node)
        else:
            dictionary[key] = value

    return dictionary

def get_consumer_id_by_custom_id(custom_id):
    url = os.environ.get("APIGATEWAY_ADMIN_INTERNAL_BASEURL") + "/consumers?custom_id=" + custom_id

    payload = {}
    headers = {
        'Content-Type': "application/json",
        'Cache-Control': "no-cache"
    }

    response = requests.request("GET", url, data=json.dumps(payload), headers=headers)
    print("ConsumerID: ", response.json().get("data",{})[0].get("id"))

    return response.json().get("data",{})[0].get("id")